﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

public class RSACryptoSystem
{
    public static string GeneratePrivateKey()
    {
        using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(2048))
        {
            return rsa.ToXmlString(true);
        }
    }

    public static string Encrypt(string plaintext, string publicKey)
    {
        using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(2048))
        {
            rsa.FromXmlString(publicKey);
            byte[] plaintextBytes = Encoding.UTF8.GetBytes(plaintext);
            byte[] encryptedBytes = rsa.Encrypt(plaintextBytes, true);
            return Convert.ToBase64String(encryptedBytes);
        }
    }

    public static string Decrypt(string ciphertext, string privateKey)
    {
        using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(2048))
        {
            byte[] encryptedBytes = Convert.FromBase64String(ciphertext);
            rsa.FromXmlString(privateKey);
            byte[] decryptedBytes = rsa.Decrypt(encryptedBytes, true);
            return Encoding.UTF8.GetString(decryptedBytes);
        }
    }

    public static void Main(string[] args)
    {
        Console.Write("Enter the plaintext: ");
        string plaintext = Console.ReadLine();

        string privateKey = GeneratePrivateKey();
        File.WriteAllText("private_key.xml", privateKey);

        string publicKey;
        using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
        {
            rsa.FromXmlString(privateKey);
            publicKey = rsa.ToXmlString(false);
        }

        string encryptedText = Encrypt(plaintext, publicKey);
        Console.WriteLine("Encrypted: " + encryptedText);

        File.WriteAllText("encrypted_data.txt", encryptedText);

        string ciphertext = File.ReadAllText("encrypted_data.txt");

       
        string decryptedText = Decrypt(ciphertext, privateKey);
        Console.WriteLine("Decrypted: " + decryptedText);
    }
}
